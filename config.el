(setq user-full-name "Daniel Ding"
      user-mail-address "rainstormstudio@yahoo.com")

(setq auto-save-default t
      confirm-kill-emacs nil
      undo-limit 80000000
      compile-command "cmake -DCMAKE_EXPORT_COMPILE_COMMANDS=ON .. && mv compile_commands.json .. && make")

(setq doom-font (font-spec :family "Fira Code" :size 14)
      doom-variable-pitch-font (font-spec :family "Ubuntu" :size 14))
(after! doom-themes
  (setq doom-themes-enable-bold t
        doom-themes-enable-italic t))

(custom-set-faces!
  '(font-lock-comment-face :slant italic))

(setq doom-theme 'doom-dracula ;'doom-dark+-customized
      doom-themes-enable-bold t
      doom-themes-enable-italic t)

(setq doom-modeline-height 22)

;(use-package! nyan-mode
;  :custom
;  (nyan-animate-nyancat t)
;  :init
;  (nyan-mode 1))

(setq fancy-splash-image "~/.doom.d/FOXHOUND.png")

(setq display-line-numbers-type t)
(display-time-mode t)

(after! org
  (setq org-directory "~/org/"
        org-agenda-files (list org-directory "~/org/roam/daily/")
        org-hide-emphasis-markers t)
)

(setq flycheck-cuda-include-path (list "."))
(setq c-default-style "linux")

(setq projectile-enable-caching nil)

(use-package! selectric-mode
  :commands selectric-mode)

(setq tramp-chunksize 2000)

(after! lsp-mode
  (setq lsp-headerline-breadcrumb-enable t))
;; (after! lsp-ui-mode
;;   (setq lsp-ui-doc-enable nil)
;;   (setq lsp-ui-doc-show-with-cursor nil)
;;   (setq lsp-ui-doc-show-with-mouse nil)
;; )

;; (setq lsp-clients-clangd-args '("-j=3"
;;                                 "--background-index"
;;                                 "--clang-tidy"
;;                                 "--completion-style=detailed"
;;                                 "--header-insertion=never"
;;                                 "--header-insertion-decorators=0"))
;; (after! lsp-clangd (set-lsp-priority! 'clangd 2))

(after! ccls
  (setq ccls-initialization-options '(:index (:comments 2) :completion (:detailedLabel t)))
  (setq lsp-lens-enable nil)
  (setq ccls-sem-highlight-method 'font-lock)
;  (setq lsp-headerline-breadcrumb-enable t)
;  (setq lsp-headerline-breadcrumb-icons-enable t)
)

(after! rustic
  (setq rustic-lsp-server 'rust-analyzer))

(after! eshell
  (defun with-face (str &rest face-plist)
    (propertize str 'face face-plist))

  (defun my-eshell-prompt ()
    (let ((header-bg "#fff"))
      (concat
       (with-face (concat (eshell/pwd) " ") :foreground "black" :background "grey")
       (with-face "\n" :foreground "grey")
       (with-face (concat "[" (user-login-name) "]") :foreground "green")
       "@"
       (with-face "[localhost]" :foreground "cyan")
       " - "
       (with-face (format-time-string "[%Y-%m-%d %H:%M] " (current-time)) :foreground "yellow")
       "\n"
       (if (= (user-uid) 0)
           (with-face "=#" :foreground "red")
         "=>")
       " ")))
    (setq eshell-prompt-function 'my-eshell-prompt)
    (setq eshell-highlight-prompt nil)
  )
